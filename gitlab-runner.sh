#!/bin/bash

# Votre URL de GitLab
GITLAB_URL="https://gitlab.com"

# Le token de registration de votre runner
REGISTRATION_TOKEN="GR1348941pbs8bR-XWYQSV3U-6PxH" 

# Le nom de votre runner
RUNNER_NAME="docker-registry-runner"

# Les tags de votre runner
RUNNER_TAGS="docker,registry"

# Installe Docker
sudo apt-get update
sudo apt-get install -y docker.io

# Lance le service Docker
sudo systemctl start docker

# Lance le runner GitLab dans un conteneur Docker
docker run -d --name $RUNNER_NAME --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

# Enregistre le runner auprès de votre instance GitLab
docker exec $RUNNER_NAME gitlab-runner register \
  --non-interactive \
  --url $GITLAB_URL \
  --registration-token $REGISTRATION_TOKEN \
  --executor "docker" \
  --docker-image docker:latest \
  --description $RUNNER_NAME \
  --tag-list $RUNNER_TAGS \
  --run-untagged="true" \
  --locked="false"
