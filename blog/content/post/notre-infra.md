---
author: "Khalil & Jawad"
date: 2023-03-25
linktitle: Explication de notre infra Docker
title: Explication de notre infra Docker
weight: 10
---

# Explication de notre infrastructure Docker

Dans le cadre de notre projet, nous avons développé une infrastructure Docker sophistiquée pour prendre en charge notre application. Cette infrastructure est conçue pour être hautement évolutive, résiliente et facilement gérable. Dans cet article, nous allons explorer les différents composants et les processus de déploiement qui font de notre infrastructure Docker une solution robuste.

## Architecture de l'infrastructure

Notre infrastructure repose sur des services Docker soigneusement conçus qui fonctionnent en harmonie pour offrir une expérience fluide et performante. Chaque service est conçu pour remplir un rôle spécifique et interagit avec les autres services de manière cohérente. Laissez-nous vous présenter les principaux composants de notre architecture :

### InfluxDB : Stockage et gestion des données

L'un des piliers de notre infrastructure est InfluxDB, une puissante base de données de séries chronologiques. Nous avons personnalisé une image Docker d'InfluxDB pour répondre à nos besoins spécifiques. InfluxDB est responsable du stockage et de la gestion des données temporelles de notre application. Pour garantir la persistance des données, nous utilisons un volume Docker pour stocker les données d'InfluxDB de manière fiable.

### Telegraf : Collecte et reporting des données

Telegraf est un agent de collecte de données qui joue un rôle crucial dans notre infrastructure. Il collecte les métriques système, les données de journal, les statistiques des conteneurs Docker, et bien plus encore. Nous avons créé une image Docker personnalisée pour Telegraf basée sur Golang. Telegraf se connecte étroitement à InfluxDB pour transmettre les données collectées, offrant ainsi des informations précieuses sur les performances et les statistiques de notre infrastructure.

### Blog Hugo : Génération de contenu statique

Pour offrir un blog dynamique et facilement extensible, nous avons adopté Hugo, un générateur de site statique populaire. Hugo nous permet de créer des pages web statiques à partir de fichiers Markdown, offrant ainsi une expérience de blogging fluide et hautement personnalisable. Nous avons personnalisé notre image Docker pour exécuter Hugo et servir les fichiers générés via un serveur web Nginx.

### Traefik : Reverse proxy et load balancer

Pour gérer le trafic entrant vers notre infrastructure, nous utilisons Traefik, un reverse proxy et un load balancer moderne. Traefik joue un rôle essentiel dans la distribution équilibrée des requêtes vers les différentes instances de notre blog Hugo. En se connectant à Docker, Traefik assure le routage efficace du trafic, garantissant ainsi une expérience utilisateur sans faille.

### Déploiement automatisé avec GitLab CI/CD

Nous avons adopté GitLab CI/CD pour automatiser le processus de construction et de déploiement de notre infrastructure Docker. GitLab CI/CD est un puissant outil d'intégration et de livraison continues qui facilite le flux de travail de développement. Grâce à notre pipeline CI/CD bien orchestré, nous pouvons construire, tester et déployer rapidement notre infrastructure.

### Pipeline CI/CD GitLab

Notre pipeline CI/CD GitLab est configuré pour exécuter différentes étapes de manière séquentielle. Nous utilisons un fichier `.gitlab-ci.yml` pour définir notre pipeline. La première étape consiste à construire et sauvegarder sur un registry les images Docker pour InfluxDB, Telegraf, le Blog et Traefik en utilisant les fichiers Dockerfile respectifs. Les images ne seront construites qu'en cas de changement dans les Dockerfile respectifs.

### Déploiement avec Ansible

Pour déployer notre infrastructure sur nos serveurs distants, nous utilisons Ansible, un outil d'automatisation puissant. Ansible nous permet de spécifier l'état souhaité de notre infrastructure dans un playbook. Nous avons créé un playbook Ansible `projet-pro.yml` qui déploie les services Docker sur les serveurs distants en utilisant les images du registre GitLab.

### Processus de déploiement

Lorsque nous lançons le processus de déploiement, GitLab CI/CD commence par construire les images Docker en utilisant les fichiers Dockerfile correspondants. Les images sont ensuite étiquetées avec un identifiant unique basé sur le numéro de Hash et latest pour faciliter la gestion et le suivi des versions.

Une fois les images construites, elles sont poussées vers notre registre GitLab privé, ce qui les rend accessibles à partir de n'importe quel serveur disposant des autorisations appropriées. Cela nous permet d'avoir un contrôle total sur nos images Docker et de gérer facilement les mises à jour et les déploiements sur nos serveurs distants.

Ensuite, Ansible est executé dans cette même pipline via le role "Deploy", il entre en jeu pour déployer les services Docker sur les serveurs distants. Nous utilisons un playbook Ansible spécifique (`projet-pro.yml`) qui définit l'état souhaité de notre infrastructure. Le playbook configure les conteneurs Docker, les réseaux, les volumes, et applique toutes les configurations nécessaires pour assurer le bon fonctionnement de notre infrastructure.

Pour simplifier l'exécution du playbook Ansible pour les différentes machines, nous avons utilisés des variables d'environnements GitLab. Ansible se charge de déployer les services Docker sur les serveurs, garantissant ainsi que notre infrastructure est en place et fonctionnelle.

Grâce à cette approche automatisée, nous pouvons déployer et mettre à jour notre infrastructure rapidement et efficacement, tout en maintenant un haut niveau de contrôle et de cohérence.

## Conclusion

Notre infrastructure Docker offre une base solide pour prendre en charge notre application avec scalabilité, fiabilité et facilité de gestion. Les composants clés tels qu'InfluxDB, Telegraf, Hugo et Traefik travaillent de concert pour assurer une expérience utilisateur optimale. Grâce à GitLab CI/CD et Ansible, nous avons automatisé le processus de déploiement, ce qui nous permet de gagner du temps et de garantir la cohérence de notre infrastructure.

*N'hésitez pas à explorer les autres articles de notre blog pour en savoir plus sur les détails techniques à venir de notre infrastructure et les meilleures pratiques que nous avons adoptées.*
