# Lancer le déploiement : 

- Afin de lancer le déploiement, il suffit de lancer manuellement la pipeline mais uniquement le stage "Deploy" en allant sur le job du dernier commit

# Lancer Le build et Push des images

- Afin de lancer les build et push des images, il y a deux façon de faire, soit ils sont lancés automatiquement après un commit où les Dockerfile ont été modifiés, soit nous les lançons manuellement en decommentant les lignes "Manual" dans chaque stage.

# Si vous souhaitez rajouter un nouvel host cible de déploiement, voici comment procéder : 

1. **Ajouter la nouvelle machine à `inventory.ini`** : Vous devez ajouter la nouvelle machine à votre fichier `inventory.ini` dans le répertoire `ansible`. Assurez-vous d'ajouter l'adresse IP de la nouvelle machine sous le bon groupe. Par exemple :

    ```ini
    [docker]
    ancienne_machine ansible_host=ancienne_machine_ip
    nouvelle_machine ansible_host=nouvelle_machine_ip
    ```

2. **Ajouter la clé SSH privée de la nouvelle machine à GitLab** : Vous devez ajouter la clé SSH privée de la nouvelle machine à vos variables d'environnement GitLab. Pour ce faire, allez dans `Settings > CI/CD > Variables` dans votre projet GitLab et ajoutez une nouvelle variable avec le nom `GITLAB_INFRA_NOUVELLE_MACHINE_PRIVATE_KEY_FILE` et la valeur étant la clé SSH privée de la nouvelle machine.

3. **Mettre à jour le fichier `.gitlab-ci.yml`** : Vous devez mettre à jour votre fichier `.gitlab-ci.yml` pour utiliser la nouvelle clé SSH lors de l'exécution du playbook Ansible. Par exemple :

    ```yaml
    deploy:
      stage: deploy
      image: docker:latest
      script:
        - apk add --update python3 python3-dev py3-pip openssl ca-certificates git openssh-client bash
        - pip3 install --upgrade pip
        - pip3 install ansible
        - mkdir -p ./keys
        - echo -e "$GITLAB_INFRA_ANCIENNE_MACHINE_PRIVATE_KEY_FILE" > ./keys/infra-ancienne_machine.pem
        - echo -e "$GITLAB_INFRA_NOUVELLE_MACHINE_PRIVATE_KEY_FILE" > ./keys/infra-nouvelle_machine.pem
        - chmod 600 ./keys/infra-*.pem
        - ansible-playbook -i ansible/inventory.ini ansible/projet-pro.yml -vvv 
      when: manual
    ```

4. **Exécuter le job `deploy`** : Une fois que vous avez effectué les étapes ci-dessus, vous pouvez exécuter le job `deploy` dans votre pipeline GitLab CI/CD. Cela déploiera votre infrastructure sur la nouvelle machine.

Veuillez noter que ce guide suppose que la carte réseau de la nouvelle machine est corréctement paramétrée et accessible via SSH à partir de votre runner GitLab CI/CD. Si ce n'est pas le cas, vous devrez effectuer ces étapes d'installation avant de pouvoir déployer votre infrastructure.

Pour toute information complémentaire sur l'infra, merci de consulter le lien [suivant](http://13.38.29.84/post/notre-infra/).

Voici la vidéo de présentation du projet : 

https://dai.ly/kKkbPVay3p5gTwzbwPW
