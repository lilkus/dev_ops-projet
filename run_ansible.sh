#!/bin/bash

# Define environment name
ENV_NAME=ansible_env

# Vérifier si Python3 est installé
if ! command -v python3 &> /dev/null
then
    echo "Python3 n'est pas installé. Installation en cours..."
    # Mise à jour de la liste des packages
    sudo apt-get update
    # Installation de Python3 et Python3-venv
    sudo apt-get install -y python3 python3-venv
else
    echo "Python3 est déjà installé."
    # Mise à jour de Python3 à la dernière version stable
    sudo apt-get upgrade -y python3
fi

# Create the virtual environment
python3 -m venv $ENV_NAME

# Activate the virtual environment
source $ENV_NAME/bin/activate

# Upgrade pip and Install required packages
python -m pip install --upgrade pip
pip install --upgrade markupsafe ansible

# Export SSH private key path
export SSH_PRIVATE_KEY=./ansible/keys/infra-docker01.pem

# Déplacer dans le répertoire ansible
cd ansible

# Lancement du playbook Ansible avec le mode verbose
sudo ansible-playbook -i inventory.ini projet-pro.yml -vvv

# Deactivate the virtual environment and remove it
deactivate
rm -rf ../$ENV_NAME
