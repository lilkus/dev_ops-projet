package powertop

import (
	"encoding/csv"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/plugins/inputs"
)

type PowerTop struct {
	Interval          float64
	IncludeSoftware   bool   `toml:"include_software"`
	CsvOutputLocation string `toml:"csv_output_location"`
}

var PowerTopConfig = `
  ## Set the interval
  #  interval = 20.0

  ## Show additional details
  # include_software = false

  ## csv file location (include the filename)
  # csv_output_location = "/usr/src/powertop.csv"
`

func (p *PowerTop) SampleConfig() string {
	return PowerTopConfig
}

func (p *PowerTop) Description() string {
	return "get power top report"
}

func (p *PowerTop) Gather(acc telegraf.Accumulator) error {
	err := GenerateCSV(p.CsvOutputLocation, p.Interval)
	if err != nil {
		return err
	}
	current := time.Now()
	if p.IncludeSoftware {
		err = CollectSoftwarePowerConsumption(acc, p.CsvOutputLocation, current)
		if err != nil {
			return err
		}
	}
	err = CollectDevicePowerMetrics(acc, p.CsvOutputLocation, current)
	if err != nil {
		return err
	}
	return nil
}

func init() {
	inputs.Add("powertop", func() telegraf.Input {
		return &PowerTop{
			Interval:          20.0,
			IncludeSoftware:   false,
			CsvOutputLocation: "/usr/src/powertop.csv",
		}
	})
}

func CollectSoftwarePowerConsumption(acc telegraf.Accumulator, csvPath string, now time.Time) error {
	output, err := ApplyFilter("sed", "-n", "/Usage;Wakeups\\/s;GPU ops\\/s;/,/^$/p", csvPath)
	if err != nil {
		return err
	}
	var total float64
	for _, row := range output[1:] {
		if row[7] == "" || row[7] == "0" {
			continue
		}
		tags := map[string]string{
			"category":     row[5],
			"description:": row[6],
		}
		fields := map[string]interface{}{
			"usage/s":                    ConvertUnits(row[0]),
			"wakeups/s":                  ConvertUnits(row[1]),
			"(software) PW estimate [W]": ConvertUnits(row[7]),
		}
		total += ConvertUnits(row[7])
		acc.AddFields("software power consumer", fields, tags, now)
	}
	acc.AddFields(
		"software power consumer",
		map[string]interface{}{
			"total power [W]": total,
		},
		map[string]string{},
		now)

	return nil
}

func CollectDevicePowerMetrics(acc telegraf.Accumulator, csvPath string, now time.Time) error {
	output, err := ApplyFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", csvPath)
	if err != nil {
		return err
	}
	for _, row := range output[1 : len(output)-1] {
		if strings.Trim(row[2], " ") == "" {
			break
		}
		tags := map[string]string{
			"device name": row[1],
		}
		fields := map[string]interface{}{
			"usage [%]":                ConvertUnits(row[0]),
			"(device) PW estimate [W]": ConvertUnits(row[2]),
		}
		acc.AddFields("device power report", fields, tags, now)
	}
	return nil
}

func GenerateCSV(file string, interval float64) error {
	_, err := exec.Command("sudo", "powertop", fmt.Sprintf("--csv=%v", file), fmt.Sprintf("--time=%v", interval)).Output()
	if err != nil {
		return err
	}
	if _, err = os.Stat(fmt.Sprintf("%v", file)); err != nil {
		return err
	}
	return nil
}

func ApplyFilter(name string, arg ...string) ([][]string, error) {
	filterOutput, err := exec.Command(name, arg...).Output()
	if err != nil {
		return nil, err
	}
	if string(filterOutput) == "" {
		return nil, fmt.Errorf("No data found")
	}
	reader := csv.NewReader(strings.NewReader(string(filterOutput)))
	reader.Comma = ';'
	reader.FieldsPerRecord = -1
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}
	return records, nil
}

func ConvertUnits(input string) float64 {
	regexResult := regexp.MustCompile(`([0-9]+\.?[0-9]*)[\s]*([a-zA-Z \/]+)?`).FindSubmatch([]byte(input))
	if len(regexResult) == 0 {
		return 0
	}
	val, err := strconv.ParseFloat(string(regexResult[1]), 64)
	if err != nil {
		fmt.Println(err)
	}
	if len(regexResult) > 2 {
		format := string(regexResult[2])
		switch {
		case strings.ContainsAny(format, "n"):
			return val * 1e-9
		case strings.ContainsAny(format, "u"):
			return val * 1e-6
		case strings.ContainsAny(format, "m"):
			return val * 1e-3
		case strings.ContainsAny(format, "k"):
			return val * 1e3
		}
	}
	return val
}
