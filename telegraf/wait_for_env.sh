#!/bin/bash

# Set the path to the .env file
ENV_FILE="/etc/telegraf/.influxdb_token"

# Set the name of the token variable
TOKEN_NAME="DOCKER_INFLUXDB_INIT_ADMIN_TOKEN"

# Wait for the .env file to exist and contain the token
while true; do
    if grep -q "^${TOKEN_NAME}=" "$ENV_FILE"; then
        break
    fi
    echo "Waiting for token to be generated..."
    sleep 5
done

# Load the environment variables
export $(grep -v '^#' "$ENV_FILE" | xargs)

# Launch Telegraf
exec telegraf
