#!/bin/bash

# Démarrer InfluxDB
influxd &

# Attendre que InfluxDB démarre
sleep 30

# Configuration initiale de InfluxDB (création de l'utilisateur admin, de l'organisation et du bucket)
response=$(curl -X POST http://localhost:8086/api/v2/setup -H 'Content-Type: application/json' -d "{\"username\": \"$USERNAME\", \"password\": \"$PASSWORD\", \"org\": \"$ORG\", \"bucket\": \"$BUCKET\"}")

# Afficher la réponse complète de l'API
echo "API Response: $response"

TOKEN=$(echo $response | jq -r '.auth.token')

# Write the token to the .env file
echo "DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=$TOKEN" >> /etc/influxdb/.influxdb_token

# Redémarrer InfluxDB pour appliquer les modifications
pkill -f influxd
sleep 2
influxd
